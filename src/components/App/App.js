import { Component } from 'react';
import Steps from '../Steps';
import Changer from '../Changer'
import './App.css';



class App extends Component{
    state = {count:0,maxValue:200,minValue:1,alter:false}


    //Selecting Min and Max Value
    updateMaxMinValue = (value)=>{
      this.setState((prevState)=>({
        maxValue: value !== prevState.maxValue ? value : prevState.maxValue,
        minValue: value > prevState.minValue && value <= prevState.maxValue? value : prevState.minValue
      }))
    
    }

    //Updateing Counter Value
    updateCountValue = (changer)=>{
      const {count,maxValue,minValue} = this.state
      
        if(changer === "increment"){
            if(Number(count)+Number(minValue) > Number(maxValue)){
              this.setState({alert:true,count:Number(maxValue)})
            }else{
              this.setState({count:Number(count)+Number(minValue)})
            }
        }else if(changer === "decrement"){
          if(count <= maxValue){
            this.setState({count:Number(count)-Number(minValue),alert:false})
          }
          
        }else if(changer === "reset"){
          this.setState({count:0,alert:false})
        }

    }


  render(){
    const {count,alert} = this.state
   
    return(
      <section>
        <h1>{count}</h1>
        {alert && <span>Reached Max Value</span>}
        <div className="steps-max-container">
          <h2>Steps</h2>
          <h2>Max Value</h2>
        </div>
          <Steps updateMaxMinValue={this.updateMaxMinValue}/>
          <Changer counter={this.updateCountValue}/>
      </section>
    )
  }
}

export default App;
