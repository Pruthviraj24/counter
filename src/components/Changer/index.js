import "./changer.css"

const Changer = (props) =>{
    const {counter} = props

    const updateCount = (event)=>{
        counter(event.target.value)
    }


    return(
        <div className="changer-container">
            <button type="button" value="increment" onClick={updateCount}>Increment</button>
            <button type="button" value="decrement" onClick={updateCount}>Decrement</button>
            <button type="button" value="reset" onClick={updateCount}>Reset</button>
        </div>
    )

}

export default Changer