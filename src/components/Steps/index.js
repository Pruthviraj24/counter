import "./steps.css"

const Steps = (props)=>{
    const {updateMaxMinValue} = props

    const selectValue = (event)=>{
        updateMaxMinValue(event.target.value)
    }


    return(
        <div className="bg-values">
            <button className="value-container" onClick={selectValue} value={5}>5</button>
            <button className="value-container" onClick={selectValue} value={10}>10</button>  
            <button className="value-container" onClick={selectValue} value={15}>15</button>
            <button className="value-container" onClick={selectValue} value={50}>50</button>
            <button className="value-container" onClick={selectValue} value={100}>100</button>
            <button className="value-container" onClick={selectValue} value={200}>200</button>
        </div>
    )

}

export default Steps